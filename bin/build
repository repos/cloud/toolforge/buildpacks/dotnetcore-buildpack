#!/usr/bin/env bash

# fail hard
set -o pipefail
# fail harder
set -eu

bp_dir=$(
	cd "$(dirname "$0")/.."
	pwd
) # absolute path
source_dir="${bp_dir}/target"

layers_dir="${1:?}"
platform_dir="${2:?}"

# translate new stack ID to old stack ID
export STACK="$CNB_STACK_ID"

# copy the buildpack source into the target dir
target_dir="$(mktemp -d)/target"
cp -R "$source_dir" "$target_dir"
chmod -R +w "$target_dir"

# create a shim cache layer
cache_dir="${layers_dir}/shim"
mkdir -p "${cache_dir}"
cat >"${layers_dir}/shim.toml" <<EOT
[types]
  launch = false
  build = true
  cache = true
EOT

"${target_dir}/bin/compile" "$(pwd)" "${cache_dir}" "${platform_dir}/env"

# copy profile.d scripts into a layer so they will be sourced
if [[ -d .profile.d ]]; then
	profile_dir="${layers_dir}/profile"
	mkdir -p "${profile_dir}/profile.d"
	cp .profile.d/* "${profile_dir}/profile.d/"
	cat >"${profile_dir}.toml" <<EOT
[types]
  launch = true
  build = false
  cache = false
EOT
fi

if [[ -f "${target_dir}/export" ]]; then
	cat >"${profile_dir}.toml" <<EOT
[types]
  launch = true
  build = true
  cache = false
EOT
	mkdir -p "${profile_dir}/env.build/"
	"${bp_dir}/bin/exports" "${target_dir}/export" "${platform_dir}" "${profile_dir}/env.build/"
fi

# run bin/release, read Procfile, and generate launch.toml
# The procfile buildpack takes care of this
#"${bp_dir}/bin/release" "${target_dir}" "${layers_dir}" "${platform_dir}" "$(pwd)"
